/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.asseco.optifarm.editor;

import com.asseco.optifarm.domain.Crop;
import com.asseco.optifarm.service.CropService;
import java.beans.PropertyEditorSupport;

/**
 * 
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
public class CropEditor extends PropertyEditorSupport {

    private CropService cropService;

    public CropEditor(CropService cropService) {
        this.cropService = cropService;
    }

    public CropEditor() { }

    @Override
    public void setAsText(String text) {
        Crop c = this.cropService.findOne(Long.valueOf(text));

        this.setValue(c);
    }
}
