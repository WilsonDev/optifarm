/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.asseco.optifarm.editor;

import com.asseco.optifarm.domain.Field;
import com.asseco.optifarm.service.FieldService;
import java.beans.PropertyEditorSupport;

/**
 * 
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
public class FieldEditor extends PropertyEditorSupport {
    
    private FieldService fieldService;
    
    public FieldEditor(FieldService fieldService) {
        this.fieldService = fieldService;
    }

    public FieldEditor() { }

    @Override
    public void setAsText(String text) {
        Field c = this.fieldService.findOne(Long.valueOf(text));

        this.setValue(c);
    }

}
