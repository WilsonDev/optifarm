/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asseco.optifarm.editor;

import com.asseco.optifarm.domain.Task;
import com.asseco.optifarm.service.TaskService;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
public class TaskEditor extends PropertyEditorSupport {

    private TaskService taskService;

    public TaskEditor(TaskService taskService) {
        this.taskService = taskService;
    }

    public TaskEditor() { }

    @Override
    public void setAsText(String text) {
        Task c = this.taskService.findOne(Long.valueOf(text));

        this.setValue(c);
    }
}
