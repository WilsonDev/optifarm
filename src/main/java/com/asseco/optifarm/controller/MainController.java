package com.asseco.optifarm.controller;

import com.asseco.optifarm.domain.*;
import com.asseco.optifarm.editor.*;
import com.asseco.optifarm.service.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Controller
public class MainController {

    @Autowired
    private FieldTaskService fieldTaskService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private FieldService fieldService;

    @Autowired
    private CropService cropService;

    @Autowired
    private PersonService personService;

    private final String[] monthNames = {"Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"};
    private final String[] dayNames = {"Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"};

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homePage(@RequestParam(value = "month", required = false, defaultValue = "") Integer month,
            @RequestParam(value = "year", required = false, defaultValue = "") Integer year,
            HttpSession session,
            ModelMap model) {

        if (personService.count() == 0) {
            personService.save(new Person());
        }

        /*    Task oranie = new Task("Orka", false, true);
            Task sianie = new Task("Siew", false, true);
            Task nawozenie = new Task("Nawożenie", false, true);
            Task sprzedaz = new Task("Sprzedaż", true, true);
            this.taskService.save(oranie);
            this.taskService.save(sianie);
            this.taskService.save(nawozenie);
            this.taskService.save(sprzedaz);

            Field sad = new Field("Sad", new BigDecimal(1), null, true);
            Field ogrod = new Field("Ogród", new BigDecimal(0.2), null, true);
            this.fieldService.save(sad);
            this.fieldService.save(ogrod);

            Crop pszenica = new Crop("Pszenica", new BigDecimal(3.2), true);
            Crop zyto = new Crop("Żyto", new BigDecimal(4.5), true);
            this.cropService.save(pszenica);
            this.cropService.save(zyto);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            try {
                this.fieldTaskService.save(new FieldTask(oranie, sad, sdf.parse("2014-05-12"), sdf.parse("2014-06-15"), new BigDecimal(2000)));
                this.fieldTaskService.save(new FieldTask(sianie, sad, sdf.parse("2014-06-5"), sdf.parse("2014-06-23"), new BigDecimal(1000)));
                this.fieldTaskService.save(new FieldTask(nawozenie, ogrod, sdf.parse("2014-06-21"), sdf.parse("2014-06-27"), new BigDecimal(500)));
            } catch (ParseException ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/

        this.panelContent(year, month, session, model);

        model.addAttribute("fieldTask", new FieldTask());

        return "panel";
    }
    
    @RequestMapping(value = "/resetDB", method = RequestMethod.GET)
    public String resetDB(ModelMap model) {
        cropService.deleteAll();
        fieldService.deleteAll();
        taskService.deleteAll();
        fieldTaskService.deleteAll();
        personService.deleteAll();
        
        return "redirect:/";
    }

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public String reportPage(@RequestParam(value = "year", required = false, defaultValue = "") Integer year, HttpSession session, ModelMap model) {
        if (year != null) {
            model.addAttribute("year", year);
        } else {
            year = (int) session.getAttribute("year");
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate;
        Date toDate;
        try {
            fromDate = sdf.parse(year + "-01-01");
            toDate = sdf.parse(year + 1 + "-01-01");

            ArrayList<FieldTask> listCostly = new ArrayList<>();
            ArrayList<FieldTask> listProfitable = new ArrayList<>();
            
            HashMap<Long, BigDecimal> countCostly = new HashMap<>();
            HashMap<Long, BigDecimal> countProfitable = new HashMap<>();

            BigDecimal totalCostly = new BigDecimal(BigInteger.ZERO);
            BigDecimal totalProfitable = new BigDecimal(BigInteger.ZERO);

            for (FieldTask fieldTask : this.fieldTaskService.yearlyTasks(fromDate, toDate)) {
                if (Objects.equals(fieldTask.getProfitable(), false)) {
                    if (!listCostly.contains(fieldTask)) {
                        listCostly.add(fieldTask);
                        countCostly.put(fieldTask.getTask().getId(), fieldTask.getTotal());
                    } else {
                        countCostly.put(fieldTask.getTask().getId(), (BigDecimal)countCostly.get(fieldTask.getTask().getId()).add(fieldTask.getTotal()));
                    }
                    totalCostly = totalCostly.add(fieldTask.getTotal());
                } else {
                    if (!listProfitable.contains(fieldTask)) {
                        listProfitable.add(fieldTask);
                        countProfitable.put(fieldTask.getId(), fieldTask.getTotal());
                    } else {
                        countProfitable.put(fieldTask.getId(), (BigDecimal)countProfitable.get(fieldTask.getId()).add(fieldTask.getTotal()));
                    }
                    totalProfitable = totalProfitable.add(fieldTask.getTotal());
                }
            }

            model.addAttribute("countProfitable", countProfitable);
            model.addAttribute("countCostly", countCostly);
            model.addAttribute("totalProfitable", totalProfitable);
            model.addAttribute("totalCostly", totalCostly);
            model.addAttribute("yearlyTasksProfitable", listProfitable);
            model.addAttribute("yearlyTasksCostly", listCostly);
        } catch (ParseException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "report";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveFieldTask(@ModelAttribute("fieldTask") @Valid FieldTask fieldTask,
            BindingResult result,
            RedirectAttributes attr,
            HttpSession session,
            ModelMap model) {

        int year = (int) session.getAttribute("year");
        int month = (int) session.getAttribute("month");

        if (result.hasErrors() || fieldTask.getFromDate().after(fieldTask.getToDate())) {
            for (int i = 0; i < result.getFieldErrors().size(); i++) {
                String fieldName = result.getFieldErrors().get(i).getField();
                if (!model.containsAttribute(fieldName)) {
                    model.addAttribute(fieldName, "has-error has-feedback");
                }
            }

            this.panelContent(year, month, session, model);

            return "panel";
        }

        if (this.taskService.findOne(fieldTask.getTask().getId()).getProfitable()) {
            BigDecimal price = this.fieldService.findOne(fieldTask.getField().getId()).getCrop().getPrice();
            if (price != null) {
                fieldTask.setTotal(fieldTask.getAmount().multiply(price));
            } else {
                fieldTask.setTotal(fieldTask.getAmount());
            }
        } else {
            BigDecimal area = this.fieldService.findOne(fieldTask.getField().getId()).getArea();
            fieldTask.setTotal(fieldTask.getAmount().multiply(area));
        }

        this.fieldTaskService.save(fieldTask);

        attr.addAttribute("year", year);
        attr.addAttribute("month", month);

        attr.addFlashAttribute("saved", fieldTask.getId());

        return "redirect:/";
    }

    @RequestMapping(value = "/edit/{fieldTaskId}", method = RequestMethod.GET)
    public String editFieldTask(@PathVariable("fieldTaskId") Long fieldTaskId,
            RedirectAttributes attr,
            HttpSession session,
            ModelMap model) {

        int year = (int) session.getAttribute("year");
        int month = (int) session.getAttribute("month");

        this.panelContent(year, month, session, model);

        model.addAttribute("fieldTask", this.fieldTaskService.findOne(fieldTaskId));

        attr.addAttribute("year", year);
        attr.addAttribute("month", month);

        return "panel";
    }

    @RequestMapping(value = "/delete/{fieldTaskId}", method = RequestMethod.GET)
    public String deleteFieldTask(@PathVariable("fieldTaskId") Long fieldTaskId,
            RedirectAttributes attr,
            HttpSession session,
            ModelMap model) {

        this.fieldTaskService.delete(this.fieldTaskService.findOne(fieldTaskId));

        int year = (int) session.getAttribute("year");
        int month = (int) session.getAttribute("month");

        attr.addAttribute("year", year);
        attr.addAttribute("month", month);

        attr.addFlashAttribute("deleted", fieldTaskId);

        return "redirect:/";
    }

    @RequestMapping(value = "/dane", method = RequestMethod.GET)
    public String person(ModelMap model) {
        model.addAttribute("person", this.personService.findOne((long) 1));

        return "person";
    }

    @RequestMapping(value = "/dane/save", method = RequestMethod.POST)
    public String person(@ModelAttribute("person") Person person, ModelMap model) {
        Person toEdit = this.personService.findOne((long) 1);

        toEdit.setName(person.getName());
        toEdit.setSurname(person.getSurname());
        toEdit.setStreet(person.getStreet());
        toEdit.setCity(person.getCity());
        toEdit.setZipCode(person.getZipCode());
        toEdit.setPhone(person.getPhone());
        toEdit.setEmail(person.getEmail());

        this.personService.save(toEdit);

        return "redirect:/dane";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Crop.class, new CropEditor(cropService));
        binder.registerCustomEditor(Task.class, new TaskEditor(taskService));
        binder.registerCustomEditor(Field.class, new FieldEditor(fieldService));

        binder.registerCustomEditor(Date.class, null, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), false));
    }

    private void panelContent(Integer year, Integer month, HttpSession session, ModelMap model) {
        Calendar currentDate = Calendar.getInstance();

        if (month == null || month == currentDate.get(Calendar.MONTH)) {
            model.addAttribute("currentDay", currentDate.get(Calendar.DAY_OF_MONTH));
        }

        if (month == null || month < 0 || month > 11) {
            month = currentDate.get(Calendar.MONTH);
        }

        if (year == null || year < 1970 || year > 2099) {
            year = currentDate.get(Calendar.YEAR);
        }

        ArrayList<Float> perc = new ArrayList<>();
        ArrayList<Float> percTo = new ArrayList<>();

        int i = 0;

        List<FieldTask> taskList = fieldTaskService.monthlyTasks(month + 1, year);

        for (FieldTask fieldTask : taskList) {
            perc.add(0f);
            percTo.add(0f);

            float[] temp = percentOfMonth(fieldTask.getFromDate(),
                    fieldTask.getToDate(), month);

            perc.set(i, temp[0]);
            percTo.set(i, temp[1]);

            i++;
        }

        session.setAttribute("year", year);
        session.setAttribute("month", month);

        model.addAttribute("dayName", dayNames[currentDate.get(Calendar.DAY_OF_WEEK) - 1]);
        model.addAttribute("monthName", monthNames[month]);

        model.addAttribute("from", perc);
        model.addAttribute("to", percTo);

        model.addAttribute("fields", this.fieldService.findActive());
        model.addAttribute("tasks", this.taskService.findActive());
        model.addAttribute("fieldTasks", taskList);
    }

    private float[] percentOfMonth(Date from, Date to, int month) {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.MONTH, month);

        Calendar fromDate = Calendar.getInstance();
        Calendar toDate = Calendar.getInstance();

        fromDate.setTime(from);
        toDate.setTime(to);

        float days = date.getActualMaximum(Calendar.DAY_OF_MONTH);
        float fromDay = fromDate.get(Calendar.DAY_OF_MONTH) - 1;
        float toDay = toDate.get(Calendar.DAY_OF_MONTH);

        float[] percent = new float[2];

        if (fromDate.get(Calendar.MONTH) < month && toDate.get(Calendar.MONTH) < month) {
            percent[0] = 0f;
            percent[1] = 0f;

            return percent;
        }

        if (fromDate.get(Calendar.MONTH) > month && toDate.get(Calendar.MONTH) > month) {
            percent[0] = 0f;
            percent[1] = 0f;

            return percent;
        }

        if (fromDate.get(Calendar.MONTH) < month && toDate.get(Calendar.MONTH) > month) {
            percent[0] = 0f;
            percent[1] = 100f;

            return percent;
        }

        percent[0] = (fromDay / days) * 100.0f;

        if (toDate.get(Calendar.MONTH) > month) {
            percent[1] = 100f - percent[0];

            return percent;
        }

        if (fromDate.get(Calendar.MONTH) < month) {
            percent[0] = 0f;
        }

        percent[1] = (toDay / days) * 100.0f - percent[0];

        return percent;
    }
}
