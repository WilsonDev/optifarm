package com.asseco.optifarm.controller;

import com.asseco.optifarm.domain.Task;
import com.asseco.optifarm.service.*;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;
    
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public String tasksPage(@RequestParam(value = "s", required = false, defaultValue = "") String search, ModelMap model) {
        Task task = new Task();
        task.setProfitable(true);
        
        model.addAttribute("task", task);
        
        if (search.equals("")) {
            search = null;
            model.addAttribute("tasks", this.taskService.findActive());
        } else {
            model.addAttribute("tasks", this.taskService.findByName(search));
        }
        
        model.addAttribute("result", search);

        return "tasks";
    }
    
    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
    public String searchTasks(@RequestParam(value = "search", required = false, defaultValue = "") String search,
            RedirectAttributes redirectAttributes) {
        if (!search.equals(""))
            redirectAttributes.addAttribute("s", search);
        
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/tasks/save", method = RequestMethod.POST)
    public String saveTask(@ModelAttribute("task") @Valid Task task,
            BindingResult result,
            RedirectAttributes attr,
            ModelMap model) {

        if (result.hasErrors()) {
            for (int i = 0; i < result.getFieldErrors().size(); i++) {
                String fieldName = result.getFieldErrors().get(i).getField();
                if (!model.containsAttribute(fieldName)) {
                    model.addAttribute(fieldName, "has-error has-feedback");
                }
            }

            //model.addAttribute("task", new Task());
            model.addAttribute("tasks", this.taskService.findActive());

            return "tasks";
        }

        task.setActive(true);
        this.taskService.save(task);

        attr.addFlashAttribute("saved", task.getId());

        return "redirect:/tasks";
    }

    @RequestMapping(value = "/tasks/edit/{taskId}", method = RequestMethod.GET)
    public String editTask(@PathVariable("taskId") Long taskId,
            ModelMap model) {

        model.addAttribute("task", this.taskService.findOne(taskId));
        model.addAttribute("tasks", this.taskService.findActive());

        return "tasks";
    }

    @RequestMapping(value = "/tasks/delete/{taskId}", method = RequestMethod.GET)
    public String deleteTask(@PathVariable("taskId") Long taskId,
            RedirectAttributes attr,
            ModelMap model) {

        Task deleted = this.taskService.findOne(taskId);
        deleted.setActive(false);

        this.taskService.save(deleted);

        attr.addFlashAttribute("deleted", taskId);

        return "redirect:/tasks";
    }
}
