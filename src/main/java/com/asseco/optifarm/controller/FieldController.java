package com.asseco.optifarm.controller;

import com.asseco.optifarm.domain.Field;
import com.asseco.optifarm.service.*;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Controller
@RequestMapping(value = "/fields")
public class FieldController {

    @Autowired
    private FieldService fieldService;

    @Autowired
    private CropService cropService;

    @RequestMapping(method = RequestMethod.GET)
    public String fieldsPage(@RequestParam(value = "s", required = false, defaultValue = "") String search, ModelMap model) {
        model.addAttribute("field", new Field());
        
        if (search.equals("")) {
            search = null;
            model.addAttribute("fields", this.fieldService.findActive());
        } else {
            model.addAttribute("fields", this.fieldService.findByName(search));
        }
        
        model.addAttribute("result", search);
        model.addAttribute("crops", this.cropService.findAll());

        return "fields";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String searchFields(@RequestParam(value = "search", required = false, defaultValue = "") String search,
            RedirectAttributes redirectAttributes) {
        if (!search.equals(""))
            redirectAttributes.addAttribute("s", search);
        
        return "redirect:/fields";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveField(@ModelAttribute("field") @Valid Field field,
            BindingResult result,
            RedirectAttributes attr,
            ModelMap model) {

        if (result.hasErrors()) {
            for (int i = 0; i < result.getFieldErrors().size(); i++) {
                String fieldName = result.getFieldErrors().get(i).getField();
                if (!model.containsAttribute(fieldName)) {
                    model.addAttribute(fieldName, "has-error has-feedback");
                }
            }

            //model.addAttribute("field", new Field());
            model.addAttribute("fields", this.fieldService.findActive());
            model.addAttribute("crops", this.cropService.findAll());

            return "fields";
        }

        if (field.getCrop().getId() == null) {
            field.setCrop(null);
        }

        field.setActive(true);
        this.fieldService.save(field);

        attr.addFlashAttribute("saved", field.getId());

        return "redirect:/fields";
    }

    @RequestMapping(value = "/edit/{fieldId}", method = RequestMethod.GET)
    public String editField(@PathVariable("fieldId") Long fieldId,
            ModelMap model) {

        model.addAttribute("field", this.fieldService.findOne(fieldId));
        model.addAttribute("fields", this.fieldService.findActive());
        model.addAttribute("crops", this.cropService.findAll());

        return "fields";
    }

    @RequestMapping(value = "/delete/{fieldId}", method = RequestMethod.GET)
    public String deleteField(@PathVariable("fieldId") Long fieldId,
            RedirectAttributes attr,
            ModelMap model) {

        Field deleted = this.fieldService.findOne(fieldId);
        deleted.setActive(false);

        this.fieldService.save(deleted);

        attr.addFlashAttribute("deleted", fieldId);

        return "redirect:/fields";
    }
}
