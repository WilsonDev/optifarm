package com.asseco.optifarm.controller;

import com.asseco.optifarm.domain.Crop;
import com.asseco.optifarm.service.*;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Controller
@RequestMapping(value = "/crops")
public class CropController {
    
    @Autowired
    private CropService cropService;
    
    @RequestMapping(method = RequestMethod.GET)
    public String cropsPage(@RequestParam(value = "s", required = false, defaultValue = "") String search, ModelMap model) {
        model.addAttribute("crop", new Crop());
        
        if (search.equals("")) {
            search = null;
            model.addAttribute("crops", this.cropService.findActive());
        } else {
            model.addAttribute("crops", this.cropService.findByName(search));
        }
        
        model.addAttribute("result", search);

        return "crops";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String searchCrops(@RequestParam(value = "search", required = false, defaultValue = "") String search,
            RedirectAttributes redirectAttributes) {
        if (!search.equals(""))
            redirectAttributes.addAttribute("s", search);
        
        return "redirect:/crops";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveCrop(@ModelAttribute("crop") @Valid Crop crop,
            BindingResult result,
            RedirectAttributes attr,
            ModelMap model) {

        if (result.hasErrors()) {
            for (int i = 0; i < result.getFieldErrors().size(); i++) {
                String fieldName = result.getFieldErrors().get(i).getField();
                if (!model.containsAttribute(fieldName)) {
                    model.addAttribute(fieldName, "has-error has-feedback");
                }
            }

            //model.addAttribute("crop", new Crop());
            model.addAttribute("crops", this.cropService.findAll());

            return "crops";
        }

        crop.setActive(true);
        this.cropService.save(crop);

        attr.addFlashAttribute("saved", crop.getId());

        return "redirect:/crops";
    }

    @RequestMapping(value = "/edit/{cropId}", method = RequestMethod.GET)
    public String editCrop(@PathVariable("cropId") Long cropId,
            ModelMap model) {

        model.addAttribute("crop", this.cropService.findOne(cropId));
        model.addAttribute("crops", this.cropService.findAll());

        return "crops";
    }

    @RequestMapping(value = "/delete/{cropId}", method = RequestMethod.GET)
    public String deleteCrop(@PathVariable("cropId") Long cropId,
            RedirectAttributes attr,
            ModelMap model) {

        Crop deleted = this.cropService.findOne(cropId);
        deleted.setActive(false);

        this.cropService.save(deleted);

        attr.addFlashAttribute("deleted", cropId);

        return "redirect:/crops";
    }
}
