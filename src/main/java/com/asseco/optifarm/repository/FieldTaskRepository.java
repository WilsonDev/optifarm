package com.asseco.optifarm.repository;

import com.asseco.optifarm.domain.FieldTask;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * 
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
public interface FieldTaskRepository extends JpaRepository<FieldTask, Long> {
    @Query("SELECT t FROM FieldTask t WHERE t.fromDate >= ?1 AND t.toDate < ?2")
    public List<FieldTask> yearlyTasks(Date fromDate, Date toDate);
}
