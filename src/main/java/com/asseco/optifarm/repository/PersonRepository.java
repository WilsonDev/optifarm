package com.asseco.optifarm.repository;

import com.asseco.optifarm.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
public interface PersonRepository extends JpaRepository<Person, Long> {

}
