/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.asseco.optifarm.repository;

import com.asseco.optifarm.domain.Field;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * 
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
public interface FieldRepository extends JpaRepository<Field, Long> {
    List<Field> findByActiveTrue();
    
    @Query("select w from Field w where lower(w.name) like ?1 and w.active = true")
    List<Field> findByName(String name);
}
