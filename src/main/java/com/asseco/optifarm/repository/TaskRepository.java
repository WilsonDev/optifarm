/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.asseco.optifarm.repository;

import com.asseco.optifarm.domain.Task;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByActiveTrue();
    
    @Query("select w from Task w where lower(w.name) like ?1 and w.active = true")
    List<Task> findByName(String name);
}
