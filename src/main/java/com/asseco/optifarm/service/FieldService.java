/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asseco.optifarm.service;

import com.asseco.optifarm.domain.Field;
import com.asseco.optifarm.repository.FieldRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Service
public class FieldService {

    @Autowired
    private FieldRepository fieldRepository;

    public List<Field> findAll() {
        return this.fieldRepository.findAll();
    }

    public List<Field> findActive() {
        return this.fieldRepository.findByActiveTrue();
    }
    
    public List<Field> findByName(String name) {
        return this.fieldRepository.findByName(name);
    }

    public Field findOne(Long id) {
        return this.fieldRepository.findOne(id);
    }

    public void save(Field field) {
        this.fieldRepository.save(field);
    }
    
    public void deleteAll() {
        this.fieldRepository.deleteAll();
    }
}
