/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asseco.optifarm.service;

import com.asseco.optifarm.domain.Person;
import com.asseco.optifarm.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public Person findOne(Long id) {
        return this.personRepository.findOne(id);
    }

    public void save(Person person) {
        this.personRepository.save(person);
    }
    
    public Long count() {
        return this.personRepository.count();
    }
    
    public void deleteAll() {
        this.personRepository.deleteAll();
    }
}
