package com.asseco.optifarm.service;

import com.asseco.optifarm.domain.FieldTask;
import com.asseco.optifarm.repository.FieldTaskRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Service
public class FieldTaskService {

    @Autowired
    private FieldTaskRepository fieldTaskRepository;

    public List<FieldTask> findAll() {
        return this.fieldTaskRepository.findAll();
    }
    
    public FieldTask findOne(Long id) {
        return this.fieldTaskRepository.findOne(id);
    }

    public void save(FieldTask fieldTask) {
        this.fieldTaskRepository.save(fieldTask);
    }

    public void delete(FieldTask fieldTask) {
        this.fieldTaskRepository.delete(fieldTask);
    }

    public Long count() {
        return this.fieldTaskRepository.count();
    }
    
    public List<FieldTask> yearlyTasks(Date fromDate, Date toDate) {
        return this.fieldTaskRepository.yearlyTasks(fromDate, toDate);
    }

    public List<FieldTask> monthlyTasks(Integer month, Integer year) {
        Calendar date = Calendar.getInstance();
        date.clear();
        date.set(year, month - 1, 1);

        Date from = date.getTime();

        date.clear();
        date.set(year, month, 1);

        Date to = date.getTime();

        ArrayList<FieldTask> tasks = new ArrayList<>();

        for (FieldTask task : this.fieldTaskRepository.findAll()) {
            if ((task.getFromDate().after(from) && task.getFromDate().before(to))
                    || (task.getToDate().after(from) && task.getToDate().before(to))
                    || (task.getFromDate().before(from) && task.getToDate().after(to))) {
                tasks.add(task);
            }
        }

        return tasks;
    }
    
    public void deleteAll() {
        this.fieldTaskRepository.deleteAll();
    }

}
