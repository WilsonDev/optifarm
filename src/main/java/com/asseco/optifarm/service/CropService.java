package com.asseco.optifarm.service;

import com.asseco.optifarm.domain.Crop;
import com.asseco.optifarm.repository.CropRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Service
public class CropService {

    @Autowired
    private CropRepository cropRepository;
    
    public List<Crop> findAll() {
        return this.cropRepository.findAll();
    }
    
    public List<Crop> findActive() {
        return this.cropRepository.findByActiveTrue();
    }
    
    public List<Crop> findByName(String name) {
        return this.cropRepository.findByName(name);
    }
    
    public Crop findOne(Long id) {
        return this.cropRepository.findOne(id);
    } 
    
    public void save(Crop crop) {
        this.cropRepository.save(crop);
    }
    
    public void delete(Crop crop) {
        this.cropRepository.delete(crop);
    }
    
    public void deleteAll() {
        this.cropRepository.deleteAll();
    }

}
