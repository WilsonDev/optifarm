/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asseco.optifarm.service;

import com.asseco.optifarm.domain.Task;
import com.asseco.optifarm.repository.TaskRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;
    
    public List<Task> findAll() {
        return this.taskRepository.findAll();
    }
    
    public List<Task> findActive() {
        return this.taskRepository.findByActiveTrue();
    }
    
    public List<Task> findByName(String name) {
        return this.taskRepository.findByName(name);
    }
    
    public Task findOne(Long id) {
        return this.taskRepository.findOne(id);
    }
    
    public void save(Task task) {
        this.taskRepository.save(task);
    }
    
    public void delete(Task task) {
        this.taskRepository.delete(task);
    }
    
    public void deleteAll() {
        this.taskRepository.deleteAll();
    }

}
