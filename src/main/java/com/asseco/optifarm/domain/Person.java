package com.asseco.optifarm.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * 
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Entity
@Table
public class Person implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Basic
    private String name;
    
    @Basic
    private String surname;
    
    @Basic
    private String street;
    
    @Basic
    private String zipCode;
    
    @Basic
    private String city;
    
    @Basic
    private String phone;
    
    @Basic
    private String email;
    
    public Person() { }
    
    public Person(String name, String surname, String street, String zipCode, String city, String phone, String email) {
        this.name = name;
        this.surname = surname;
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
        this.phone = phone;
        this.email = email;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
