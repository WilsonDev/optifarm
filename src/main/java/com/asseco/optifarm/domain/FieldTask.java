package com.asseco.optifarm.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Entity
@Table
public class FieldTask implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Task task;

    @ManyToOne
    private Field field;

    @Basic
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(javax.persistence.TemporalType.DATE)
    @NotNull
    private Date fromDate;

    @Basic
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(javax.persistence.TemporalType.DATE)
    @NotNull
    private Date toDate;

    @Basic
    @NotNull
    private BigDecimal amount;
    
    @Basic
    private BigDecimal total;
    
    @Basic
    private Boolean profitable;

    public FieldTask() {
    }

    public FieldTask(Task task, Field field, Date fromDate, Date toDate, BigDecimal amount) {
        this.task = task;
        this.field = field;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.amount = amount;
    }
    
    @Override
    public boolean equals(Object object) {
        boolean isSame = false;

        if (object != null && object instanceof FieldTask){
            isSame = Objects.equals(this.task.getId(), ((FieldTask) object).task.getId());
        }

        return isSame;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
    
    public Boolean getProfitable() {  
        this.profitable = this.task.getProfitable();
        
        return profitable;
    }

}
