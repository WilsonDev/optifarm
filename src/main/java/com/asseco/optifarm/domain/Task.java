package com.asseco.optifarm.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
 */
@Entity
@Table
public class Task implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    @NotEmpty
    private String name;
    
    @Basic
    private String description;

    @Basic
    @NotNull
    private Boolean profitable;
    
    @Basic
    private Boolean active;
    
    public Task() { }

    public Task(String name, Boolean profitable, Boolean active) {
        this.name = name;
        this.profitable = profitable;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getProfitable() {
        return profitable;
    }

    public void setProfitable(Boolean profitable) {
        this.profitable = profitable;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
