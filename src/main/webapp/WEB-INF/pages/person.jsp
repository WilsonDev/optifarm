<%-- 
    Document   : user
    Created on : 2014-04-27, 12:10:37
    Author     : Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OPTIFARM</title>

        <link href="${contextPath}/assets/css/bootstrap.css" rel="stylesheet">
        <link href="${contextPath}/assets/css/panel.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">

            <div class="navbar navbar-default navbar-strip" role="navigation"></div>
            <div class="navbar navbar-inverse navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">Optifarm</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/" ><i class="glyphicon glyphicon-calendar"></i> Harmonogram</a>
                            </li>
                            <li class="dropdown">
                                <a href="/fields" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-home"></i> Gospodarstwo <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/fields">Pola uprawne</a></li>
                                    <li><a href="/crops">Uprawy</a></li>
                                    <li><a href="/tasks">Typy zadań</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/report"><i class="glyphicon glyphicon-list-alt"></i> Raporty</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown active">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i> Ustawienia <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/dane">Edycja danych</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </div><!--/.navbar -->

            <div class="row">

                <div class="col-md-3 sidebar" id="sidebar" role="navigation">
                    <div class="list-group">
                        <div class="list-group-item">
                            <address>
                                <strong>${person.name} ${person.surname}</strong><br>
                                ${person.street}<br>
                                ${person.zipCode}, ${person.city}<br>
                                <abbr title="Telefon">T:</abbr> ${person.phone}<br>
                                <a href="mailto:#">${person.email}</a>
                            </address>
                        </div>
                    </div><!--/.list-group -->
                </div><!--/.sidebar-->

                <div class="col-md-9">
                    
                    <div class="row">
                        <form:form method="post" action="/dane/save" commandName="person">
                            <div class="col-md-6">
                                <h2 class="sub-header">Użytkownik</h2>
                                <hr>
                                <div class="form-group">
                                    <form:label path="name">Imię</form:label>
                                    <form:input path="name" type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <form:label path="surname">Nazwisko</form:label>
                                    <form:input path="surname" type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <form:label path="street">Adres</form:label>
                                    <form:input path="street" type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <form:label path="zipCode">Kod pocztowy</form:label>
                                    <form:input path="zipCode" type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <form:label path="city">Miejscowość</form:label>
                                    <form:input path="city" type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <form:label path="phone">Telefon</form:label>
                                    <form:input path="phone" type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <form:label path="email">E-mail</form:label>
                                    <form:input path="email" type="text" class="form-control" />
                                </div>
                            </div>
                            <!--<div class="col-md-6">
                                <h2 class="sub-header">Gospodarstwo</h2>
                                <hr>
                                <div class="form-group">
                                    <label>Nazwa</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Ulica</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Kod pocztowy</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Miejscowość</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>-->
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Aktualizuj</button>
                                <button type="submit" class="btn btn-default">Anuluj</button>
                            </div>
                        </form:form>
                    </div>

                </div><!--/span-->
            </div><!--/row-->

            <hr>

            <footer>
                <p>© Optifarm 2014</p>
            </footer>

        </div><!--/.container-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="${contextPath}/assets/js/bootstrap.min.js"></script>
    </body>
</html>
