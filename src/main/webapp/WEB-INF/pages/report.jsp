<%-- 
    Document   : report
    Created on : 2014-06-13, 15:16:30
    Author     : Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OPTIFARM</title>

        <link href="${contextPath}/assets/css/bootstrap.css" rel="stylesheet">
        <link href="${contextPath}/assets/css/panel.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">

            <div class="navbar navbar-default navbar-strip" role="navigation"></div>
            <div class="navbar navbar-inverse navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">Optifarm</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/" ><i class="glyphicon glyphicon-calendar"></i> Harmonogram</a>
                            </li>
                            <li class="dropdown">
                                <a href="/fields" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-home"></i> Gospodarstwo <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/fields">Pola uprawne</a></li>
                                    <li><a href="/crops">Uprawy</a></li>
                                    <li><a href="/tasks">Typy zadań</a></li>
                                </ul>
                            </li>
                            <li class="active">
                                <a href="/report"><i class="glyphicon glyphicon-list-alt"></i> Raporty</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i> Ustawienia <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/dane">Edycja danych</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </div><!--/.navbar -->

            <div class="row">

                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <a href="/report/?year=${year-1}" class="btn btn-default" style="margin-top: 20px; margin-bottom: 10px;"><i class="glyphicon glyphicon-chevron-left"></i></a>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <h2 class="sub-header text-center">${year}</h2>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <a href="/report/?year=${year+1}" class="btn btn-default pull-right" style="margin-top: 20px; margin-bottom: 10px;"><i class="glyphicon glyphicon-chevron-right"></i></a>
                        </div>
                    </div><!--/.row -->

                    <hr>

                    <h2><small>Koszty</small></h2>

                    <hr>

                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="col-sm-6 col-xs-6">Operacja</th>
                                    <th class="col-sm-6 col-xs-6">Kwota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="fieldTask" items="${yearlyTasksCostly}" varStatus="count">
                                    <tr>
                                        <td>${fieldTask.task.name}</td>
                                        <td>${countCostly[fieldTask.task.id]}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <thead>
                                <tr>
                                    <th>Suma</th>
                                    <th>${totalCostly} zł</th>
                                </tr>
                            </thead>
                        </table>

                        <h2><small>Przychody</small></h2>

                        <hr>

                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="col-sm-6 col-xs-6">Operacja</th>
                                    <th class="col-sm-6 col-xs-6">Kwota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="fieldTask" items="${yearlyTasksProfitable}">
                                    <tr>
                                        <td>${fieldTask.task.name}</td>
                                        <td>${fieldTask.total}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <thead>
                                <tr>
                                    <th>Suma</th>
                                    <th>${totalProfitable} zł</th>
                                </tr>
                            </thead>
                        </table>

                        <h2><small>Razem</small></h2>

                        <hr>

                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="col-sm-6 col-xs-6">Wynik finansowy</th>
                                    <th class="col-sm-6 col-xs-6">${totalProfitable - totalCostly} zł</th>
                                </tr>
                            </thead>
                        </table>
                    </div><!--/.table-responsive -->
                </div><!--/span-->
            </div><!--/row-->

            <hr>

            <footer>
                <ul class="list-inline">
                    <li><p>© Optifarm 2014</p></li>
                    <li class="pull-right"><p></p></li>
                </ul>
            </footer>

        </div><!--/.container-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="${contextPath}/assets/js/bootstrap.min.js"></script>
    </body>
</html>
