<%-- 
    Document   : crops
    Created on : 2014-05-13, 00:05:31
    Author     : Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OPTIFARM</title>

        <link href="${contextPath}/assets/css/bootstrap.css" rel="stylesheet">
        <link href="${contextPath}/assets/css/panel.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">

            <div class="navbar navbar-default navbar-strip" role="navigation"></div>
            <div class="navbar navbar-inverse navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">Optifarm</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/" ><i class="glyphicon glyphicon-calendar"></i> Harmonogram</a>
                            </li>
                            <li class="dropdown active">
                                <a href="/fields" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-home"></i> Gospodarstwo <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/fields">Pola uprawne</a></li>
                                    <li><a href="/crops">Uprawy</a></li>
                                    <li><a href="/tasks">Typy zadań</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/report"><i class="glyphicon glyphicon-list-alt"></i> Raporty</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i> Ustawienia <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/dane">Edycja danych</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </div><!--/.navbar -->

            <div class="row">

                <div class="col-md-3 sidebar" id="sidebar" role="navigation">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Nowa uprawa</h3>
                        </div>
                        <div class="panel-body">
                            <form:form method="post" action="/crops/save" commandName="crop">
                                <form:hidden path="id" />
                                <div class="form-group ${name}">
                                    <form:label path="name">Nazwa</form:label>
                                    <form:input path="name" autocomplete="off" class="form-control" />
                                    <c:if test="${!empty name}">
                                        <span data-toggle="tooltip" title='<form:errors path="name"/>' class="glyphicon glyphicon-remove form-control-feedback error-tooltip"></span>
                                    </c:if>
                                </div>
                                <div class="form-group ${price}">
                                    <form:label path="price">Cena skupu</form:label>
                                    <form:input path="price" autocomplete="off" class="form-control" />
                                    <c:if test="${!empty price}">
                                        <span data-toggle="tooltip" title='<form:errors path="price"/>' class="glyphicon glyphicon-remove form-control-feedback error-tooltip"></span>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    <form:label path="description">Opis</form:label>
                                    <form:textarea path="description" rows="3" autocomplete="off" class="form-control" />
                                </div>
                                <c:choose>
                                    <c:when test="${!empty crop.id}">
                                        <button type="submit" class="btn btn-primary">Zapisz</button>
                                        <a href="/crops" class="btn btn-default">Anuluj</a>
                                    </c:when>
                                    <c:otherwise>
                                        <button type="submit" class="btn btn-default">Dodaj</button>
                                    </c:otherwise>
                                </c:choose>
                            </form:form>
                        </div>
                    </div><!--/.list-group -->
                    
                    <!--<div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Nowa uprawa</h3>
                        </div>
                        <div class="panel-body">
                            
                        </div>
                    </div>-->
                </div><!--/.sidebar-->

                <div class="col-md-9">
                    
                    <c:if test="${!empty saved}">
                        <div class="alert alert-success alert-dismissable fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Uwaga!</strong> Zapisano uprawę o ID: ${saved}
                        </div>
                    </c:if>

                    <c:if test="${!empty crop.id}">
                        <div class="alert alert-info">
                            <strong>Uwaga!</strong> Dokonujesz edycji uprawy o ID: ${crop.id}
                        </div>
                    </c:if>

                    <c:if test="${!empty deleted}">
                        <div class="alert alert-danger alert-dismissable fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Uwaga!</strong> Usunięto uprawę o ID: ${deleted}
                        </div>
                    </c:if>

                    <div class="row">
                        <div class="col-md-8 col-xs-8">
                            <h2 class="sub-header">Typy upraw <c:if test="${result != null}"><small>Wyniki dla "${result}"</small></c:if></h2>  
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <form method="post">
                                <div class="input-group" style="margin-top: 20px; margin-bottom: 10px;">
                                    <input name="search" class="form-control">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>

                    <hr>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nazwa</th>
                                <th>Cena</th>
                                <th>Opis</th>
                                <th>Operacje</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="crop" items="${crops}">
                                <tr>
                                    <td>${crop.id}</td>
                                    <td>${crop.name}</td>
                                    <td>${crop.price}</td>
                                    <td>${crop.description}</td>
                                    <td>
                                        <a href="/crops/edit/${crop.id}" class="btn btn-primary btn-sm">Edytuj</a>
                                        <a href="/crops/delete/${crop.id}" class="btn btn-danger btn-sm">Usuń</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                </div><!--/span-->
            </div><!--/row-->

            <hr>

            <footer>
                <p>© Optifarm 2014</p>
            </footer>

        </div><!--/.container-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="${contextPath}/assets/js/bootstrap.min.js"></script>
        
        <script type="text/javascript">
            $(function() {
                $('.error-tooltip').tooltip({
                    placement: 'right',
                    container: 'body',
                    html: true
                });
            });
            
            $(".alert").alert();
        </script>
    </body>
</html>
