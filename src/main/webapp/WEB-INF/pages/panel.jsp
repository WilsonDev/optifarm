<%-- 
    Document   : panel
    Created on : 2014-04-22, 13:02:36
    Author     : Łukasz Szypliński <Lukasz.Szyplinski@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OPTIFARM</title>

        <link href="${contextPath}/assets/css/bootstrap.css" rel="stylesheet">
        <link href="${contextPath}/assets/css/panel.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">

            <div class="navbar navbar-default navbar-strip" role="navigation"></div>
            <div class="navbar navbar-inverse navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">Optifarm</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="/" ><i class="glyphicon glyphicon-calendar"></i> Harmonogram</a>
                            </li>
                            <li class="dropdown">
                                <a href="/fields" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-home"></i> Gospodarstwo <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/fields">Pola uprawne</a></li>
                                    <li><a href="/crops">Uprawy</a></li>
                                    <li><a href="/tasks">Typy zadań</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/report"><i class="glyphicon glyphicon-list-alt"></i> Raporty</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i> Ustawienia <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/dane">Edycja danych</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#resetConfirm">Resetuj bazę danych</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </div><!--/.navbar -->

            <div class="modal fade" id="resetConfirm" tabindex="-1" role="dialog" aria-labelledby="resetConfirmLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="resetConfirmLabel">Uwaga</h4>
                        </div>
                        <div class="modal-body">
                            Operacja usunie wszystkie dane.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                            <a href="/resetDB" class="btn btn-primary">OK</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-3 sidebar" id="sidebar" role="navigation">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Nowe zadanie</h3>
                        </div>
                        <div class="panel-body">
                            <form:form method="post" action="/save" commandName="fieldTask">
                                <form:hidden path="id" />
                                <div class="form-group">
                                    <form:label path="task.id">Zadanie</form:label>
                                        <div class="input-group">
                                        <form:select path="task.id" cssClass="form-control" itemLabel="name" itemValue="id">
                                            <c:forEach var="task" items="${tasks}">
                                                <form:option value="${task.id}">${task.name}</form:option>
                                            </c:forEach>
                                        </form:select>
                                        <span class="input-group-btn">
                                            <a href="/tasks" data-toggle="tooltip" title="Dodaj nowy typ zadania" class="btn btn-default error-tooltip"><i class="glyphicon glyphicon-plus"></i></a>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group ${fromDate}">
                                    <form:label path="fromDate">Data rozpoczęcia</form:label>
                                    <form:input path="fromDate" autocomplete="off" class="form-control" placeholder="yyyy-MM-dd" />
                                    <c:if test="${!empty fromDate}">
                                        <span data-toggle="tooltip" title='<form:errors path="fromDate"/>' class="glyphicon glyphicon-remove form-control-feedback error-tooltip"></span>
                                    </c:if>
                                </div>
                                <div class="form-group ${toDate}">
                                    <form:label path="toDate">Data zakończenia</form:label>
                                    <form:input path="toDate" autocomplete="off" class="form-control" placeholder="yyyy-MM-dd" />
                                    <c:if test="${!empty toDate}">
                                        <span data-toggle="tooltip" title='<form:errors path="toDate"/>' class="glyphicon glyphicon-remove form-control-feedback error-tooltip"></span>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    <form:label path="field.id">Pole</form:label>
                                        <div class="input-group">
                                        <form:select path="field.id" cssClass="form-control" itemLabel="name" itemValue="id">
                                            <c:forEach var="field" items="${fields}">
                                                <form:option value="${field.id}"><c:out value="${field.name} ${field.area}"/></form:option>
                                            </c:forEach>
                                        </form:select>
                                        <span class="input-group-btn">
                                            <a href="/fields" data-toggle="tooltip" title="Dodaj nowe pole" class="btn btn-default error-tooltip"><i class="glyphicon glyphicon-plus"></i></a>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group ${amount}">
                                    <form:label path="amount">Wartość</form:label>
                                        <div class="input-group">
                                        <form:input path="amount" autocomplete="off" class="form-control" />
                                        <!--<c:if test="${!empty amount}">
                                            <span data-toggle="tooltip" title='<form:errors path="amount"/>' class="glyphicon glyphicon-remove form-control-feedback error-tooltip"></span>
                                        </c:if>-->
                                        <span class="input-group-addon error-tooltip" data-toggle="tooltip" title="Jeżeli wybrane zostało zadanie, które przynosi koszty to jest to wartość wyrażona w zł/ha">
                                            <i class="glyphicon glyphicon-info-sign"></i>
                                        </span>
                                    </div>
                                </div>
                                <c:choose>
                                    <c:when test="${!empty fieldTask.id}">
                                        <button type="submit" class="btn btn-primary">Zapisz</button>
                                        <a href="/" class="btn btn-default">Anuluj</a>
                                    </c:when>
                                    <c:otherwise>
                                        <button type="submit" class="btn btn-default">Dodaj</button>
                                    </c:otherwise>
                                </c:choose>
                            </form:form>
                        </div>
                    </div><!--/.list-group -->
                </div><!--/.sidebar-->

                <div class="col-md-9">
                    <c:if test="${!empty saved}">
                        <div class="alert alert-success alert-dismissable fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Uwaga!</strong> Zapisano zadanie o ID: ${saved}
                        </div>
                    </c:if>

                    <c:if test="${!empty fieldTask.id}">
                        <div class="alert alert-info">
                            <strong>Uwaga!</strong> Dokonujesz edycji zadania o ID: ${fieldTask.id}
                        </div>
                    </c:if>

                    <c:if test="${!empty deleted}">
                        <div class="alert alert-danger alert-dismissable fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Uwaga!</strong> Usunięto zadanie o ID: ${deleted}
                        </div>
                    </c:if>

                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <c:choose>
                                <c:when test="${month == 0}">
                                    <a href="/?year=${year-1}&month=${11}" class="btn btn-default" style="margin-top: 20px; margin-bottom: 10px;"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    </c:when>
                                    <c:otherwise>
                                    <a href="/?year=${year}&month=${month-1}" class="btn btn-default" style="margin-top: 20px; margin-bottom: 10px;"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    </c:otherwise>
                                </c:choose>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <h2 class="sub-header text-center">${monthName} <small>${year}</small></h2>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <c:choose>
                                <c:when test="${month == 11}">
                                    <a href="/?year=${year+1}&month=${0}" class="btn btn-default pull-right" style="margin-top: 20px; margin-bottom: 10px;"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    </c:when>
                                    <c:otherwise>
                                    <a href="/?year=${year}&month=${month+1}" class="btn btn-default pull-right" style="margin-top: 20px; margin-bottom: 10px;"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    </c:otherwise>
                                </c:choose>
                        </div>
                    </div><!--/.row -->

                    <hr>

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="col-sm-2 col-xs-2">Zadanie</th>
                                    <th class="col-sm-10 col-xs-10">
                            <div id="timeline" class="progress"></div>
                            <c:if test="${currentDay != null}">
                                <div id="current-day" class="progress" data-toggle="popover" data-content='<h2 style="margin-top: 10px;">${currentDay} <small>${dayName}</small></h2>'></div>
                            </c:if>
                            </th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${fieldTasks}" var="fieldTask" varStatus="count">
                                    <tr>
                                        <td>${fieldTask.task.name}</td>
                                        <td>
                                            <div class="progress progress-transparent task">
                                                <div class="progress-bar progress-bar-transparent" style="width: ${from[count.index]}%">
                                                    <span class="sr-only">Start</span>
                                                </div>
                                                <div class="progress-bar progress-popover" style="width: ${to[count.index]}%; 
                                                     <c:choose>
                                                         <c:when test="${fieldTask.id / 5.0 == 0.2}">background-color: #5bc0de;</c:when>
                                                         <c:when test="${fieldTask.id / 5.0 == 0.4}">background-color: #f0ad4e;</c:when>
                                                         <c:when test="${fieldTask.id / 5.0 == 0.6}">background-color: #5cb85c;</c:when>
                                                         <c:when test="${fieldTask.id / 5.0 == 0.8}">background-color: #d9534f;</c:when>
                                                         <c:otherwise>background-color: #428bca;</c:otherwise>
                                                     </c:choose>" data-toggle="popover" 

                                                     data-content='<h4>${fieldTask.task.name}</h4>
                                                     <dl class="dl-horizontal">
                                                     <dt>Od:</dt>
                                                     <dd><fmt:formatDate value="${fieldTask.fromDate}" pattern="yyyy-MM-dd" /></dd>
                                                     <dt>Do:</dt>
                                                     <dd><fmt:formatDate value="${fieldTask.toDate}" pattern="yyyy-MM-dd" /></dd>
                                                     <dt>Pole:</dt>
                                                     <dd>${fieldTask.field.name}</dd>
                                                     <dt>Pow.:</dt>
                                                     <dd>${fieldTask.field.area} ha</dd>
                                                     <c:choose>
                                                         <c:when test="${fieldTask.task.profitable}">
                                                             <dt>Przychód:</dt>
                                                         </c:when>
                                                         <c:otherwise>
                                                             <dt>Koszt:</dt>
                                                         </c:otherwise>
                                                     </c:choose>
                                                     <dd>${fieldTask.total} zł</dd></dl>
                                                     <a href="/edit/${fieldTask.id}" class="btn btn-primary">Edytuj</a>
                                                     <a href="/delete/${fieldTask.id}" class="btn btn-danger pull-right">Usuń</a>'>
                                                    <span class="sr-only">End</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div><!--/.table-responsive -->
                </div><!--/span-->
            </div><!--/row-->

            <hr>

            <footer>
                <ul class="list-inline">
                    <li><p>© Optifarm 2014</p></li>
                    <li class="pull-right"><p></p></li>
                </ul>
            </footer>

        </div><!--/.container-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="${contextPath}/assets/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $('.progress-popover').popover({
                html: true,
                placement: 'bottom'
            });

            $('.progress-popover').on('click', function(e) {
                $('.progress-popover').not(this).popover('hide');
            });

            $('#current-day').popover({
                html: true,
                placement: 'bottom'
            });

            $(function() {
                $('.error-tooltip').tooltip({
                    placement: 'right',
                    container: 'body',
                    html: true
                });
            });

            $(".alert").alert();

            $(document).ready(function() {
                timeline();
            });

            function timeline() {
                var day = parseInt('${currentDay}');
                var month = parseInt('${month}') + 1;
                var year = parseInt('${year}');

                var days = new Date(year, month, 0);
                var pixels = (31 - days.getDate()) * 23;
                var width = 713 - pixels;

                if (days.getDate() < 30) {
                    pixels = pixels - 23;
                    var margin = "0px " + pixels + "px 0px 0px";

                    var els = document.getElementsByClassName("task");
                    for (var i = 0; i < els.length; i++) {
                        els[i].style.margin = margin;
                    }
                }

                var dayPosition = day * 23 - 23;

                if (day === 1) {
                    document.getElementById("current-day").style.borderBottomLeftRadius = '4px';
                    document.getElementById("current-day").style.borderTopLeftRadius = '4px';
                }

                if (day === days.getDate()) {
                    document.getElementById("current-day").style.borderBottomRightRadius = '4px';
                    document.getElementById("current-day").style.borderTopRightRadius = '4px';
                }

                document.getElementById("current-day").style.marginLeft = dayPosition + 'px';
                document.getElementById("timeline").style.width = width + 'px';
            }
        </script>
    </body>
</html>
